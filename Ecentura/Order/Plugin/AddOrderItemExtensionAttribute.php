<?php

namespace Ecentura\Order\Plugin;

use Magento\Sales\Api\OrderItemRepositoryInterface;

class AddOrderItemExtensionAttribute
{
    /**
     * @var \Magento\Sales\Api\Data\OrderItemExtensionFactory
     */
    private $orderItemExtensionFactory;

    public function __construct(
        \Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtensionFactory
    ) {
        $this->orderItemExtensionFactory = $orderItemExtensionFactory;
    }

    public function afterGetList(
        OrderItemRepositoryInterface $subject,
        $result,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    ) {
        foreach ($result->getItems() as $item) {
            $options = $item->getProductOptions();
            $infoBuyRequest = $options['info_buyRequest'] ?? null;
            if (!$infoBuyRequest) {
                continue;
            }

            $additionalOptions = $infoBuyRequest['additional_options'] ?? null;
            if (!$additionalOptions) {
                continue;
            }

            $orderItemExtension = $item->getExtensionAttributes() ?: $this->orderItemExtensionFactory->create();
            $skuSap = isset($additionalOptions['sku_sap_return_value']) ? $additionalOptions['sku_sap_return_value']['value'] : null;
            if ($skuSap) {
                $orderItemExtension->setSkuSapReturnValue($skuSap);
            }

            $colorNumber = isset($additionalOptions['colornumber']) ? $additionalOptions['colornumber']['value'] : null;
            if ($colorNumber) {
                $orderItemExtension->setColornumber($colorNumber);
            }
            $item->setExtensionAttributes($orderItemExtension);
        }

        return $result;
    }
}